" vundle-required base configuration
" vim is older than I am; so vi compatibility seems unnecessary.
set nocompatible
filetype off

" load vundle
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" manage vundle with vundle (required by vundle)
Bundle 'gmarik/vundle'

" load bundles from GitHub with vundle
Bundle 'mileszs/ack.vim'
Bundle 'markschabacker/cocoa.vim'
Bundle 'kien/ctrlp.vim'
Bundle 'myusuf3/numbers.vim'
Bundle 'tpope/vim-markdown'
Bundle 'tpope/vim-fugitive'
Bundle 'vim-scripts/taglist.vim'
Bundle 'basilgor/vim-autotags'


" always use zsh inside vim
set shell=zsh

" enable syntax colouring and filetype detection.
syntax enable
filetype plugin indent on

" customize status line
set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P

set showcmd                       " Display incomplete commands.
set showmode                      " Display the mode you're in.

set backspace=indent,eol,start    " Intuitive backspacing.

set hidden                        " Handle multiple buffers better.

set wildmenu                      " Enhanced command line completion.
set wildmode=list:longest         " Complete files like a shell.
set wildignore+=*.pyc

set ignorecase                    " Case-insensitive searching.
set smartcase                     " But case-sensitive if expression contains a capital letter.

set relativenumber                " Relative line number
set ruler                         " Show cursor position.

set incsearch                     " Highlight matches as you type.
set hlsearch                      " Highlight matches.

set scrolloff=3                   " Show 3 lines of context around the cursor.
set wrap                          " Wrap automatically.
set linebreak                     " Don't wrap in the middle of words.
set colorcolumn=81                " Display guide at the 80th column

set visualbell                    " No beeping.

set nobackup                      " Don't make a backup before overwriting a file.
set nowritebackup                 " And again.
set directory=$HOME/.vim/tmp//,.  " Keep swap files in one location

set shiftwidth=4                  " Set the tab width
set tabstop=4                     " No seriously, set the tab width.
set noexpandtab                   " Use hard tabs instead of spaces.

" Intelligently switch between tab settings on a per–FT basis.
" Keep `* setlocal` consistent with the above if you value your sanity.
au FileType * setlocal shiftwidth=4 tabstop=4 noexpandtab
au FileType coffee,ruby setlocal shiftwidth=2 tabstop=2 expandtab

set cursorline

set title                         " Set the terminal's title

set gdefault                      " Default regexes to global
set laststatus=2


" Remaps

" Remap leader to ','
let mapleader=","

" Use 'jj' to exit insert mode
inoremap jj <ESC>l

" Shift-Enter to exit from insert mode
inoremap <S-CR> <Esc>l

" Use tab to jump between do/end etc.
nnoremap <tab> %
vnoremap <tab> %

" Quicker command mode
nnoremap ; :

" Hit Ctrl+F to toggle search highlights
nnoremap <C-f> :set hlsearch!<CR>

" Window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Navigate through visual lines, not logical lines.
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk

" Use ',a' to run an Ack search.
nnoremap <leader>a :Ack<space>

" reload vimrc with ,rc
nnoremap <leader>rc :source $MYVIMRC<return>

" reload gvimrc with ,rg
nnoremap <leader>rg :source $MYGVIMRC<return>

" Appearance
syntax enable
set background=dark
colorscheme tomorrow-night-eighties

"hi clear CursorLine
"au InsertEnter * hi CursorLine guibg=#073642
"au InsertLeave * hi CursorLine guibg=#05323d
"hi CursorLine guibg=#05323d

hi Cursor guibg=white
hi Visual guibg=#333333 guifg=#EEEEEE
hi ColorColumn guibg=#222222

" Misc

" Remember last location in file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal g'\"" | endif
endif

" Thorfile, Rakefile, Vagrantfile and Gemfile are Ruby
au BufRead,BufNewFile {Gemfile,Rakefile,Vagrantfile,Thorfile,config.ru} set ft=ruby

" .zsh-theme files are Zsh
au BufRead,BufNewFile {*.zsh-theme} set ft=zsh

" MacVIM shift+arrow-keys behavior (required in .vimrc)
let macvim_hig_shift_movement = 1

""""""""""""""""""""
" ctrl-p
" keybindings
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

" search in the root directory of the current project
let g:ctrlp_working_path_mode = 'rw'

" open files created with ctrl-y in the current panel
let g:ctrlp_open_new_file = 'r'


""""""""""""""""""""
" numbers.vim
" keybindings
nnoremap <leader>ln :NumbersToggle<CR>


""""""""""""""""""""
" Kill trailing whitespace
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>

" Open a new split and switch to it.
nnoremap <leader>w <C-w>v<C-w>l
nnoremap <leader>s <C-w>s<C-w>j

