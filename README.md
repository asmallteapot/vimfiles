## Vim with Teapot characteristics
My primary motivations in configuring Vim are twofold:

1. Sanity: I want to live in this editor every day. Simple stuff should be easy.

2. Portability: Configuration should work reasonably well in terminal and GUI 
environments alike.

There might be some useful stuff in here if you share either of those goals, 
or if you're also a Mac user.

You may also be interested in my [Vim bookmarks on Pinboard][pinboard].

[pinboard]: https://pinboard.in/u:asmallteapot/t:vim/


### Screenshot
[![Screenshot][screenshot]](http://tcups.me/K9Bq)

[screenshot]: http://tcups.me/K9Bq/Screen%20Shot%202012-10-14%20at%2016.28.24.png


### MacVim icon
I'm using an [alternate MacVim icon designed by Drew Yeaton][macvim-icon] on my 
OS X machines.

![Icon][macvim-icon-preview]

[macvim-icon]: http://dribbble.com/shots/121306-MacVim-Replacement-Icon
[macvim-icon-preview]: http://dribbble.s3.amazonaws.com/users/2086/screenshots/121306/shot_1298917103.png

### Font
I'm currently using Menlo 14pt in MacVim (and in terminals).

